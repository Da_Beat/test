let productosObtenidos;

function getProductos() {
  let url = 'https://services.odata.org/V4/Northwind/Northwind.svc/Products';
  let request = new XMLHttpRequest();

  request.onreadystatechange = function () {
    if (this.readyState == 4 || this.status == 200) {
      // console.table(JSON.parse(request.responseText).value);
      productosObtenidos = request.responseText;
      procesarProductos(productosObtenidos);
    };
  };

  request.open('GET', url, true); // true - Petición síncrona
  request.send();

};

function procesarProductos(productosObtenidos) {
  let JSONProductos = JSON.parse(productosObtenidos).value;
  // alert(JSONProductos.value[0].ProductName);
  let divTabla = document.getElementById('divTablaProductos');
  let tabla = document.createElement('table');
  let tbody = document.createElement('tbody');
  tabla.classList.add('table', 'table-hover');

  for (let i = 0; i < JSONProductos.length; i++) {
    // console.log(JSONProductos[i].ProductName);
    let nuevaFila = document.createElement('tr');

    let columnaNombre = document.createElement('td');
    columnaNombre.innerText = JSONProductos[i].ProductName;

    let columnaPrecio = document.createElement('td');
    columnaPrecio.innerText = `$${JSONProductos[i].UnitPrice}`;

    let columnaStock = document.createElement('td');
    columnaStock.innerText = `${JSONProductos[i].UnitsInStock} unidades`;

    nuevaFila.appendChild(columnaNombre);
    nuevaFila.appendChild(columnaPrecio);
    nuevaFila.appendChild(columnaStock);

    tbody.appendChild(nuevaFila);
  };
  tabla.appendChild(tbody);
  divTabla.appendChild(tabla);

};