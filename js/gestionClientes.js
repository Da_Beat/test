let clientesObtenidos;

function getClientes() {
  let url = "https://services.odata.org/V4/Northwind/Northwind.svc/Customers" // ?$filter=Country eq 'Germany'";
  let request = new XMLHttpRequest();

  request.onreadystatechange = function () {
    if (this.readyState == 4 || this.status == 200) {
      // console.table(JSON.parse(request.responseText).value);
      clientesObtenidos = request.responseText;
      procesarClientes(clientesObtenidos);
    };
  };

  request.open('GET', url, true); // true - Petición síncrona
  request.send();

};

function procesarClientes(clientesObtenidos) {
  let JSONClientes = JSON.parse(clientesObtenidos).value;
  let urlBandera = 'https://www.countries-ofthe-world.com/flags-normal/flag-of-';

  let divTabla = document.getElementById('divTablaClientes');
  let tabla = document.createElement('table');
  let tbody = document.createElement('tbody');
  tabla.classList.add('table', 'table-hover');

  for (let i = 0; i < JSONClientes.length; i++) {
    let pais = JSONClientes[i].Country;

    let nuevaFila = document.createElement('tr');

    let columnaNombre = document.createElement('td');
    columnaNombre.innerText = JSONClientes[i].ContactName;

    let columnaCiudad = document.createElement('td');
    columnaCiudad.innerText = `${JSONClientes[i].City}`;

    let columnaPais = document.createElement('td');
    columnaPais.innerText = `${pais}`;

    let imgBandera = document.createElement('img');
    imgBandera.className = 'flag';

    if (pais == 'UK') {
      imgBandera.src = `${urlBandera}United-Kingdom.png`;
    } else {
      imgBandera.src = `${urlBandera}${pais}.png`;
    };
    
    nuevaFila.appendChild(columnaNombre);
    nuevaFila.appendChild(columnaCiudad);
    nuevaFila.appendChild(columnaPais);
    nuevaFila.appendChild(imgBandera);

    tbody.appendChild(nuevaFila);
  };
  tabla.appendChild(tbody);
  divTabla.appendChild(tabla);

};